﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Data;
using System.Data.Odbc;
using MySql.Data.MySqlClient;

namespace Hubflow
{
    class Class1
    {
        static String connectionString = "Data Source=172.27.63.53;Initial Catalog=LAPS_UAT;User ID=SA;Password=M4ll1ng$";

        static String sql = null;

        static String hapikey = "ab6a64df-0a2f-4280-bc21-f7e09348cc24"; 
        static String resLink;
        static String ts = "";
        //static String updatejson = "";
        static int count = 10;
        static int match = 0;
        static int num = 0;
        static int lapscount = 0;
        static string lapsupdate = "no";
        static int upLoadLength = 0;
        static int offset;
        static double time_offset;
        static int c = 0;
        static string byEmails;
        static string emailx;
        static string Opt;
        static string cid;
        static string lid;
        static string upLoad = "";
        static string upBody = "";
        static string fileName = "";
        static FileStream fs;
        static int bcount;
        static string bdate;
        static string bstart;
        static string bfinish;
        static string password = "root"; // "pestana2017!"; // 
        static string username = "root"; //"niyi"; // 

        static HttpResponseMessage response;
        static HttpResponseMessage responseEmail;


        //Get Data, Client object. Post Data, httpClientpost
        static HttpClient client = new HttpClient();
        static HttpClient httpClientpost = new HttpClient();


        static void Main(string[] args)
        {
            Updatebatch("", "");
            ParseContacts();
            HubBatch(-1, 0);
            Console.ReadLine();
        }


        static async void HubBatch(int vidOffset, double timeOffset)
        {
            Console.WriteLine();
            Console.WriteLine("Hub Start");
            Console.WriteLine();

            bstart = DateTime.Now.ToString("HH mm ss");
            bdate = DateTime.Now.ToString("yyyy MM dd");

            // Call asynchronous network methods in a try/catch block to handle exceptions
            try
            {
    
                //********************************************************************
                //Get recently updated and created contacts
                //********************************************************************

                if (vidOffset == -1)
                {
                    Console.WriteLine();
                    //resLink = "https://api.hubapi.com/contacts/v1/lists/all/contacts/all?hapikey=" + hapikey + "&count=" + count;
                    resLink = "https://api.hubapi.com/contacts/v1/lists/recently_updated/contacts/recent?hapikey=" + hapikey + "&count=" + count + "&property=hub_a_addfromdate&property=hub_a_dependantno&property=hub_a_dob&property=email&property=hub_a_employmentstatus&property=hub_a_employmenttown&property=hub_a_isloggedin&property=form_section_4&property=form_section_5&property=firstname&property=lastname&property=hub_a_lengthofloan&property=hub_a_lastchanged&property=hub_a_mobile&property=mobilephone&property=loan_id&property=loannumber&property=loan_stage&property=hub_a_maritalstatus&property=hub_a_marketing_consent&property=hub_a_monthlytakehome&property=hub_a_postcode&property=hub_a_title&property=hub_a_whenareyoupaid";
                    response = await client.GetAsync(resLink);
                }
                else
                {
                    Console.WriteLine();
                    //resLink = "https://api.hubapi.com/contacts/v1/lists/all/contacts/all?hapikey=" + hapikey + "&count=" + count + "&vidOffset=" + vidOffset;
                    resLink = "https://api.hubapi.com/contacts/v1/lists/recently_updated/contacts/recent?hapikey=" + hapikey + "&count=" + count + "&property=hub_a_addfromdate&property=hub_a_dependantno&property=hub_a_dob&property=email&property=hub_a_employmentstatus&property=hub_a_employmenttown&property=hub_a_isloggedin&property=form_section_4&property=form_section_5&property=firstname&property=lastname&property=hub_a_lengthofloan&property=hub_a_lastchanged&property=hub_a_mobile&property=mobilephone&property=loan_id&property=loannumber&property=loan_stage&property=hub_a_maritalstatus&property=hub_a_marketing_consent&property=hub_a_monthlytakehome&property=hub_a_postcode&property=hub_a_title&property=hub_a_whenareyoupaid" + "&vidOffset=" + vidOffset + "&timeOffset=" + timeOffset;
                    response = await client.GetAsync(resLink);
                }

                response.EnsureSuccessStatusCode();
                String responseBody = await response.Content.ReadAsStringAsync();
                // Above three lines can be replaced with new helper method below
                // string responseBody = await client.GetStringAsync(uri);

                JObject contacts = JObject.Parse(responseBody);
                String vidoffset = @"vid-offset";
                String has_more = @"has-more";
                String timeoffset = @"time-offset";

                offset = (int)(contacts.GetValue(vidoffset));
                has_more = (string)contacts.GetValue(has_more);
                time_offset = (double)(contacts.GetValue(timeoffset));



                //*******************************************************************
                //  Extract Timestamps From Hubspot Contacts
                //*******************************************************************

                for (c = 0; c < count; c++)
                {
                    //var contact = "contacts[" + c + "].identity-profiles[0].identities";
                    var conn = contacts.SelectToken("contacts[" + c + "].properties.lastmodifieddate.value");
                    var conn1 = contacts.SelectToken("contacts[" + c + "]");

                    var today = DateTime.Now;
                    var yesterday = today.AddDays(-10);
                    var dateTimeOffset = new DateTimeOffset(yesterday);
                    var unixDateTime = dateTimeOffset.ToUnixTimeMilliseconds();

                    if (conn != null)
                    {

                        if ((long)unixDateTime < (long)conn)
                        {
                            num++;
                            //Console.WriteLine("-24hrs: " + yesterday + " ts:  " + unixDateTime);
                            Console.WriteLine(conn1);
                            ts = ts + "\r\n" + conn1.ToString() + ",";
                        }
                    }
                }


                Console.WriteLine();

                c = 0;



                if (string.Compare(has_more, "True") == 0)
                {
                    System.IO.File.WriteAllText(@"C:\Users\noluwa\Documents\Live_Output.txt", ts);
                    //Console.WriteLine(has_more);
                    Console.WriteLine(num + " On to the next.");
                    Console.WriteLine(offset);
                    Console.WriteLine(time_offset);
                    //UpdateGroup()
                    //UpdateHub(upLoad);
                    HubBatch((int)offset, (double)time_offset);
                }
                else
                {
                    //Write one string to a text file.
                    System.IO.File.WriteAllText(@"C:\Users\noluwa\Documents\Live_Output.txt", ts);

                    Console.WriteLine("Done");
                    //Batch(bcount, bdate, bstart, bfinish, match.ToString(), count.ToString(), lapscount.ToString());
                    Console.WriteLine("Found {0} entries ", num);
                    Console.WriteLine(ts);
                    Console.ReadLine();
                }

                /*******************************************************************************************************************/

            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                Console.ReadLine();
            }
        }

        public static DateTime UnixTimestampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

     

     


        //***************************************************************/
        // UpdateHubspot get Current Batch number 
        //

        public static int BatchNo()
        {
            string query = "SELECT COUNT(*) FROM hub_batch";
            string connect = "SERVER=localhost;DATABASE=hubspot_log;UID=" + username + ";PASSWORD=" + password;
            MySqlConnection connection = new MySqlConnection(connect);
            //open connection
            connection.Open();

            //create command and assign the query and connection from the constructor
            MySqlCommand cmd = new MySqlCommand(query, connection);
            Int32 batchcount = Convert.ToInt32(cmd.ExecuteScalar());

            connection.Close();
            return batchcount;
        }


        /***************************************************************/
        // UpdateHubMarketingConcent with Laps content
        //

        public static String ParseContacts()
        {
            string jsonData = @"{  
                    'FirstName':'Jignesh',  
                    'LastName':'Trivedi'  
                    }";
            dynamic data = JObject.Parse(jsonData);

            Console.WriteLine(string.Concat("Hi ", data.FirstName, " " + data.LastName));
            //Console.ReadLine();

            string text = System.IO.File.ReadAllText(@"C:\Users\noluwa\Documents\inputText.txt");

            // Display the file contents to the console. Variable text is a string.
            //Console.WriteLine("Contents of inputText.txt = {0}", text);


            string jsonData2 = "{'contacts': [{" +
                    "'email':'test35@hotmail.nommy'," +
                    "'properties': [" +
                    "{'property':'firstname','value': 'Test'}," +
                    "{'property':'lastname','value': 'two'}," +
                    "{'property':'website','value': 'http://hubspot.com'}," +
                    "{'property':'company','value': 'HubSpot'}," +
                    "{'property':'phone','value': '02086714258'}," +
                    "{'property':'address','value': '25 First Street'}," +
                    "{'property':'city','value': 'London'}," +
                    "{'property':'state','value': 'MA'}," +
                    "{'property':'zip','value': '02139'}" +
                    "]" +
                    "}," +
                    "{" +
                    "'email':'Nice@hotmail.nommy'," +
                    "'properties': [" +
                    "{'property':'firstname','value': 'Test'}," +
                    "{'property':'lastname','value': 'two'}," +
                    "{'property':'website','value': 'http://hubspot.com'}," +
                    "{'property':'company','value': 'HubSpot'}," +
                    "{'property':'phone','value': '555-122-2323'}," +
                    "{'property':'address','value': '25 First Street'}," +
                    "{'property':'city','value': 'London'}," +
                    "{'property':'state','value': 'MA'}," +
                    "{'property':'zip','value': '02139'}" +
                    "]" +
                    "}]}";

            //JObject data2 = JObject.Parse("{'People':[{'Name':'Jeff'},{'Name':'Joe'}]}");
            JObject data2 = JObject.Parse(jsonData2);
            string name = (string)data2.SelectToken("contacts[0].properties[4].value");
            string jame = (string)data2.SelectToken("contacts[1].email");


            Console.WriteLine(name);
            Console.WriteLine(data2.SelectToken("contacts[0]"));


            JObject data3 = JObject.Parse(text);
            Console.WriteLine("Contents of inputText.txt = {0}", data3.SelectToken("contacts[0]"));
            Console.ReadLine();

            return null;
        }



        /*******************************************************************************************************************/
        // UpdateMarketingConcent

        /*******************************************************************************************************************/

        public static async void UpdateMarketingConcent(string cid, string lid)
        {
            string OutputField = string.Empty;

            httpClientpost = new HttpClient();
            try
            {
                string resourceAddress = "https://auat.unclebuck.tv/api/AuxiliaryServices/AddPersonNote";

                string apiPostBody = "{" +
                    "\"LoanId\": \"" + lid + "\"," +
                    "\"Note\": \"Marketing Decision\"," +
                    "\"UserId\": \"3\"," +
                    "\"NoteLevelId\": \"2\"," +
                    "\"NoteHeaderId\": \"" + cid + "\"}";

                httpClientpost.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage wcfResponse = await httpClientpost.PostAsync(resourceAddress, new StringContent(apiPostBody, Encoding.UTF8, "application/json"));
                await DisplayTextResult(wcfResponse, OutputField);
            }
            catch (HttpRequestException hre)
            {
                NotifyUser("Error:" + hre.Message);
            }
            catch (TaskCanceledException)
            {
                NotifyUser("Request canceled.");
            }
            catch (Exception ex)
            {
                NotifyUser(ex.Message);
            }
        }



        /*******************************************************************************************************************/
        // Create or update a group of contacts
        // Note: The batch size should not exceed 1000 contacts per request.
        /*******************************************************************************************************************/

        public static async void Updatebatch(string cid, string lid)
        {
            string OutputField = string.Empty;

            httpClientpost = new HttpClient();
            try
            {
                string resourceAddress = "https://api.hubapi.com/contacts/v1/contact/batch/?hapikey=" + hapikey;

                string apiPostUpdate = "[" +


                    "{" +
                    "\"email\":\"test35@hotmail.com\"," +
                    "\"properties\": [" +
                    "{\"property\":\"firstname\",\"value\": \"Test\"}," +
                    "{\"property\":\"lastname\",\"value\": \"two\"}," +
                    "{\"property\":\"website\",\"value\": \"http://hubspot.com\"}," +
                    "{\"property\":\"company\",\"value\": \"HubSpot\"}," +
                    "{\"property\":\"phone\",\"value\": \"555-122-2323\"}," +
                    "{\"property\":\"address\",\"value\": \"25 First Street\"}," +
                    "{\"property\":\"city\",\"value\": \"London\"}," +
                    "{\"property\":\"state\",\"value\": \"MA\"}," +
                    "{\"property\":\"zip\",\"value\": \"02139\"}" +
                    "]" +
                    "}" +

                    "]";

                httpClientpost.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage wcfResponse = await httpClientpost.PostAsync(resourceAddress, new StringContent(apiPostUpdate, Encoding.UTF8, "application/json"));
                await DisplayTextResult(wcfResponse, OutputField);
            }
            catch (HttpRequestException hre)
            {
                NotifyUser("Error:" + hre.Message);
            }
            catch (TaskCanceledException)
            {
                NotifyUser("Request canceled.");
            }
            catch (Exception ex)
            {
                NotifyUser(ex.Message);
            }
        }

        /*******************************************************************************************************************/
        // Notify User

        /*******************************************************************************************************************/

        private static void NotifyUser(string Note)
        {
            Console.WriteLine(Note);
        }


        /*******************************************************************************************************************/
        // DisplayTextResult from Api call

        /*******************************************************************************************************************/

        static async Task DisplayTextResult(HttpResponseMessage Message, string OutputField)
        {
            string responsJsonText = await Message.Content.ReadAsStringAsync();

            Console.WriteLine("responsJsonText = {0}", responsJsonText);
        }
    }
}


