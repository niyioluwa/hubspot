﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Data;
using System.Data.Odbc;
using MySql.Data.MySqlClient;

namespace Hubflow
{
    class Program
    {  
        static String hapikey = "95d43732-4262-4cd8-8504-9b2c18a588d0";  //"0fbf899e-d306-4e65-85d3-f45793c6bd2f"; //  "0fbf899e-d306-4e65-85d3-f45793c6bd2f"; //  "ab6a64df-0a2f-4280-bc21-f7e09348cc24"; //     HUB_ID 4157382 
        static String resLink;

        static int count = 100;                         // Return Batch count
        static int BatchCountNumber = 0;              // BatchCountNumber
        static int num = 0;
        static int checkloop = 0;
        static int checkstart = 0;
        static int checkstop = 0;
        static int offset;                            // Contact offset
        static double time_offset;                    // Time offset
        static int c = 0;                             // batch count
        static int lc = 0;                            // loop count

        // Retrived hub time fields
        static double conn_createdate;                // contacts.SelectToken("contacts[" + c + "].identity-profiles[0].saved-at-timestamp");
        static double conn_lastmodifieddate;          // contacts.SelectToken("contacts[" + c + "].properties.lastmodifieddate.value");

        // Datatable upload
        static DataTable dtblup = new DataTable();    // Datatable with the set of recently updated contacts to be sent to Uncle buck Database
        static DataTable dtbl = new DataTable();      // Datatable with modified details to update Hubspot recored 

        // DateTime upload
        static DateTime Checkdate = DateTime.Now;     // DateTime of Checkdate
        static DateTime last = DateTime.Now;          // DateTime of Last Start (Function)
        static int HubSpotStartId = 0;                // ID of StartTime

        // Update hub fields
        static string H_Vid;                          // Vid
        static string P_mktOptIn;                     // marketing opt-in
        static string P_sumFundedLoan;                // Life time value
        static string P_nbFundedLoan;                 // Number of loans
        static string LF_LoanNo;                      // Loan number
        static string LF_Instalments;                 // Loan length
        static string LF_Balance;                     // outstanding balance 
        static string LF_FundedDate;                  // funded date
        static string LF_LastInstDate;                // due date
        static string LF_RepaidDate;                  // repaid date
        static string LF_High_ArrearsLevel;           // ArrearsLevel
        static string LA_StageStatus;                 // StageStatus
        static string LA_ExcludeFromLending;          // excluded from lending 


        // Upload data 
        static string vid;
        static string vids;
        static string contact;                          // Returned formatted contact data from datatable 
        static string contacts;                         // Total Returned formatted contact data from datatable 
        //static int rowcount;                         // Returned Number of contacts in contact
        static bool fin = false;


        //static string password = "root";             // "pestana2017!"; // 
        //static string username = "root";             // "niyi"; // 

        static HttpResponseMessage response;
        //static HttpResponseMessage responseEmail;


        //Get Data, Client object. Post Data, httpClientpost
        static HttpClient client = new HttpClient();
        static HttpClient httpClientpost = new HttpClient();
        //static SqlConnection sqlcon = new SqlConnection(@"Data Source=172.27.63.53;Initial Catalog=JosefTestingDB_UAT;User ID=SA;Password=M4ll1ng$");
        static SqlConnection sqlcon = new SqlConnection(@"Data Source=172.27.63.23;Initial Catalog=JosefTestingDB;User ID=SA;Password=u3zD2Ix6");

        //test variables
        //"test53@test.com" || dtblup.Rows[i][column.ColumnName].ToString() == "c40p97k6r3n38lz0zq@uncle-buck.co.uk"
        //static int testEmail = 0;  //test53@test.com
        //static int funkyEmail = 0; //c40p97k6r3n38lz0zq@uncle-buck.co.uk
        //test variables
        static DateTime startTime = DateTime.Now;
        static int runs = 0;



        //************************************************************************************************************************************************
        //  0.  Main
        //      
        //    
        //      
        //************************************************************************************************************************************************


        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Cyan;


            // Create upload Table
            dtblup = new DataTable();
            dtblup.Columns.Add("Vid", typeof(int));
            dtblup.Columns.Add("Email", typeof(string));
            dtblup.Columns.Add("Firstname", typeof(string));
            dtblup.Columns.Add("Surname", typeof(string));
            dtblup.Columns.Add("marketing_consent", typeof(string));
            dtblup.Columns.Add("createdate", typeof(DateTime));
            dtblup.Columns.Add("lastmodifieddate", typeof(DateTime));


            //Get Last updated
            HubBatch2(-1, 0);  //  int vidOffset, double timeOffset)

            //Put into Datatable

            //Push to database

            //Parse Data from stored Procedure

            //Exitpro(3);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Please exit application...");
            Console.ReadLine();
        }

        static void Exitpro(int t)
        {
            for (int s = t; s > -1; s--)
            {
                System.Threading.Thread.Sleep(1000);
                if (s == 0)
                {                
                    //HubSpotEnd(sqlcon, DateTime.Now, HubSpotStartId);
                    Console.Write("\rGood bye.     ");
                }
                else
                {
                    Console.Write("\rSleep in {0}      ", s);
                }
                
            }
        }


        //************************************************************************************************************************************************
        //  1.  Hubloop!
        //      Get Last Updated 
        //    
        //      Return Json
        //************************************************************************************************************************************************

        //static async void HubBatch(int vidOffset, double timeOffset)
        static void HubBatch(int vidOffset, double timeOffset)
        {
            if (runs % 50 == 0)
            {
                Console.WriteLine();
                Console.WriteLine("************************************************************************************");
                System.TimeSpan diffResult = startTime.Subtract(DateTime.Now);
                Console.WriteLine("seconds run: " + diffResult.Seconds.ToString());
                Console.WriteLine("minutes run: " + diffResult.Minutes.ToString());
                Console.WriteLine("runs: " + runs.ToString());
            }
            runs++;

            // Call asynchronous network methods in a try/catch block to handle exceptions
            try
            {
                //********************************************************************
                //Get recently updated and created contacts
                //********************************************************************
                if (vidOffset == -1)
                {
                    // 6.5 HubSpotStart
                    //Console.WriteLine("getting last start date");
                    //Console.WriteLine(DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                   // Checkdate = HubSpotLastStart(sqlcon);            // Last start date
                    //Console.WriteLine("got last start date");
                    //Console.WriteLine(DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                    //Console.WriteLine("Checkdate:  {0}", Checkdate);
                    
                    var hss = HubSpotStart(sqlcon, DateTime.Now);    // Start date and time  

                    HubSpotStartId = hss;                            // Start date and time id 
                    BatchCountNumber++;

                    
                    //resLink = "https://api.hubapi.com/contacts/v1/lists/all/contacts/all?hapikey=" + hapikey + "&count=" + count;
                    //resLink = "https://api.hubapi.com/contacts/v1/lists/recently_updated/contacts/recent?hapikey=" + hapikey + "&count=" + count + "&property=hub_a_addfromdate&property=hub_a_dependantno&property=hub_a_dob&property=email&property=hub_a_employmentstatus&property=hub_a_employmenttown&property=hub_a_isloggedin&property=form_section_4&property=form_section_5&property=firstname&property=lastname&property=hub_a_lengthofloan&property=hub_a_lastchanged&property=hub_a_mobile&property=mobilephone&property=loan_id&property=loannumber&property=loan_stage&property=hub_a_maritalstatus&property=hub_a_marketing_consent&property=hub_a_monthlytakehome&property=hub_a_postcode&property=hub_a_title&property=hub_a_whenareyoupaid";
                    //resLink = "https://api.hubapi.com/contacts/v1/lists/all/contacts/all?hapikey=" + hapikey + "&count=" + count + "&property=vid&property=email&property=firstname&property=lastname&property=hub_a_marketing_consent&property=lastmodifielddate";
                    
                    resLink = "https://api.hubapi.com/contacts/v1/lists/recently_updated/contacts/recent?hapikey=" + hapikey + "&count=" + count + "&property=vid&property=email&property=firstname&property=lastname&property=hub_a_marketing_consent&property=lastmodifielddate";
                    //response = await client.GetAsync(resLink);
                    response = client.GetAsync(resLink).Result;
                }
                else
                {
                    BatchCountNumber++;

                    
                    //resLink = "https://api.hubapi.com/contacts/v1/lists/all/contacts/all?hapikey=" + hapikey + "&count=" + count + "&vidOffset=" + vidOffset;
                    //resLink = "https://api.hubapi.com/contacts/v1/lists/recently_updated/contacts/recent?hapikey=" + hapikey + "&count=" + count + "&property=hub_a_addfromdate&property=hub_a_dependantno&property=hub_a_dob&property=email&property=hub_a_employmentstatus&property=hub_a_employmenttown&property=hub_a_isloggedin&property=form_section_4&property=form_section_5&property=firstname&property=lastname&property=hub_a_lengthofloan&property=hub_a_lastchanged&property=hub_a_mobile&property=mobilephone&property=loan_id&property=loannumber&property=loan_stage&property=hub_a_maritalstatus&property=hub_a_marketing_consent&property=hub_a_monthlytakehome&property=hub_a_postcode&property=hub_a_title&property=hub_a_whenareyoupaid" + "&vidOffset=" + vidOffset + "&timeOffset=" + timeOffset;
                    //resLink = "https://api.hubapi.com/contacts/v1/lists/all/contacts/all?hapikey=" + hapikey + "&count=" + count + "&property=vid&property=email&property=firstname&property=lastname&property=hub_a_marketing_consent&property=lastmodifielddate" + "&vidOffset=" + vidOffset;
                    
                    resLink = "https://api.hubapi.com/contacts/v1/lists/recently_updated/contacts/recent?hapikey=" + hapikey + "&count=" + count + "&property=vid&property=email&property=firstname&property=lastname&property=hub_a_marketing_consent&property=lastmodifielddate" + "&vidOffset=" + vidOffset + "&timeOffset=" + timeOffset;
                    //response = await client.GetAsync(resLink);
                    response = client.GetAsync(resLink).Result;
                }

                response.EnsureSuccessStatusCode();
                //String responseBody = await response.Content.ReadAsStringAsync();
                String responseBody = response.Content.ReadAsStringAsync().Result;
                // Above three lines can be replaced with new helper method below
                // string responseBody = await client.GetStringAsync(uri);

                JObject contacts = JObject.Parse(responseBody);
                String vidoffset = @"vid-offset";
                String has_more = @"has-more";
                String timeoffset = @"time-offset";

                offset = (int)(contacts.GetValue(vidoffset));
                has_more = (string)contacts.GetValue(has_more);
                time_offset = (double)(contacts.GetValue(timeoffset));

                //******************************************************************
                //  Batch Conversion
                //
                //******************************************************************

                //Console.WriteLine("building table");
                // Fill Datatable with each received batch
                for (c = 0; c < count; c++)
                {
                    var conn = contacts.SelectToken("contacts[" + c + "].properties.lastmodifieddate.value");
                    var conn1 = contacts.SelectToken("contacts[" + c + "]");

                    if (conn != null)
                    {  
                        DateTime date1 = UnixTimestampToDateTime((double)conn);    // And lastupdated in HubSpot
                        //int result = DateTime.Compare(date1, Checkdate);           // var Checkdate comes from HubSpotLastStart(sqlcon);
                        // int result = 1;
                        
                        if (runs % 2000 == 0)
                        {
                            //Console.WriteLine("12601 found");
                            Console.Write(contacts.SelectToken("contacts[" + c + "].vid") + " -> ");
                            Console.Write(contacts.SelectToken("contacts[" + c + "].properties.email.value") + " -> ");
                            Console.Write(contacts.SelectToken("contacts[" + c + "].properties.firstname.value") + " -> ");
                            Console.Write(contacts.SelectToken("contacts[" + c + "].properties.lastname.value") + " -> ");
                            Console.Write(contacts.SelectToken("contacts[" + c + "].properties.hub_a_marketing_consent.value") + " -> ");
                            Console.Write(contacts.SelectToken("contacts[" + c + "].identity-profiles[0].saved-at-timestamp") + " -> ");
                            Console.WriteLine(contacts.SelectToken("contacts[" + c + "].properties.lastmodifieddate.value"));
                        }


                        num++;
                        var conn_vid = contacts.SelectToken("contacts[" + c + "].vid");
                        var conn_email = contacts.SelectToken("contacts[" + c + "].properties.email.value");
                        var conn_first = contacts.SelectToken("contacts[" + c + "].properties.firstname.value");
                        var conn_last = contacts.SelectToken("contacts[" + c + "].properties.lastname.value");
                        var conn_Market_Consent = contacts.SelectToken("contacts[" + c + "].properties.hub_a_marketing_consent.value");
                        //if (conn_Market_Consent == null)
                        //    conn_Market_Consent = "false";
                        conn_createdate = (double)contacts.SelectToken("contacts[" + c + "].identity-profiles[0].saved-at-timestamp");
                        conn_lastmodifieddate = (double)contacts.SelectToken("contacts[" + c + "].properties.lastmodifieddate.value");

                        // 6.3  Add to Datatable
                        GetHubAppDataTableAll((int)conn_vid, (string)conn_email, (string)conn_first, (string)conn_last, (string)conn_Market_Consent, UnixTimestampToDateTime(conn_createdate), UnixTimestampToDateTime(conn_lastmodifieddate));     
                    }                
                }
                Console.WriteLine();


                HubAppBulkInsertionSection(sqlcon, dtblup);
                dtblup.Rows.Clear();
                c = 0;
                
                
                //*******************************************************************
                //  Upload to Analytics table
                //
                //*******************************************************************


                if (string.Compare(has_more,"True") == 0)
                {
                    //Console.WriteLine("{0} Found, On to the next.", num);
                    //Console.WriteLine("more to add to table");
                    HubBatch((int)offset,(double)time_offset);
                }
                else
                {
                    Console.WriteLine("***** Pre Insert Datatable *****");
                    Console.WriteLine("Retrieved All Recently Updated ");
                    Console.WriteLine("Found {0} entries ", num);

                    // Read Datatable dtblup
                    //********************************************************
                    Console.WriteLine();
                    
                    Console.WriteLine("The last updated contacts have been retrieved");
                    Console.WriteLine("Datatable data table has been created from the recieved json");
                    //Console.WriteLine("DataTable From ReceivedJson In Database ***");
                    Console.WriteLine();
                    //********************************************************

                    // 6.4   Put DataTable (dtblup) From  ReceivedJson In Database
                    //********************************************************

                    Console.WriteLine("calling bulk Insert Checkloop:  {0}", checkloop);
                    HubAppBulkInsertionAll(sqlcon, dtblup);
                    Console.WriteLine("called bulk insert");

                    Console.WriteLine("Inserting Checkloop {0}", checkloop);
                    Console.WriteLine("Counting how many Checkstarts Occur:  {0}", checkstart);


                }

                /**********************************************************************/

            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                Console.ReadLine();
            }
        }


        //************************************************************************************************************************************************
        //  2.  Hubloop!
        //      Get Last Updated 
        //    
        //      Return Json
        //************************************************************************************************************************************************

        //static async void HubBatch(int vidOffset, double timeOffset)
        static void HubBatch2(int vidOffset, double timeOffset)
        {
            while(lc == 0){
                if (runs % 50 == 0)
                {
                    Console.WriteLine();
                    Console.WriteLine("************************************************************************************");
                    System.TimeSpan diffResult = startTime.Subtract(DateTime.Now);
                    Console.WriteLine("seconds run: " + diffResult.Seconds.ToString());
                    Console.WriteLine("minutes run: " + diffResult.Minutes.ToString());
                    Console.WriteLine("runs: " + runs.ToString());
                }
                runs++;

                // Call asynchronous network methods in a try/catch block to handle exceptions
                try
                {
                    //********************************************************************
                    //Get recently updated and created contacts
                    //********************************************************************
                    Console.WriteLine("vidoffset {0}", vidOffset);
                    if (vidOffset == -1)
                    {
                        // 6.5 HubSpotStart
                        Console.WriteLine("getting last start date");
                        //Console.WriteLine(DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        // Checkdate = HubSpotLastStart(sqlcon);            // Last start date
                        //Console.WriteLine("got last start date");
                        //Console.WriteLine(DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        //Console.WriteLine("Checkdate:  {0}", Checkdate);

                        var hss = HubSpotStart(sqlcon, DateTime.Now);    // Start date and time  

                        HubSpotStartId = hss;                            // Start date and time id 
                        BatchCountNumber++;


                        //resLink = "https://api.hubapi.com/contacts/v1/lists/all/contacts/all?hapikey=" + hapikey + "&count=" + count;
                        //resLink = "https://api.hubapi.com/contacts/v1/lists/recently_updated/contacts/recent?hapikey=" + hapikey + "&count=" + count + "&property=hub_a_addfromdate&property=hub_a_dependantno&property=hub_a_dob&property=email&property=hub_a_employmentstatus&property=hub_a_employmenttown&property=hub_a_isloggedin&property=form_section_4&property=form_section_5&property=firstname&property=lastname&property=hub_a_lengthofloan&property=hub_a_lastchanged&property=hub_a_mobile&property=mobilephone&property=loan_id&property=loannumber&property=loan_stage&property=hub_a_maritalstatus&property=hub_a_marketing_consent&property=hub_a_monthlytakehome&property=hub_a_postcode&property=hub_a_title&property=hub_a_whenareyoupaid";
                        //resLink = "https://api.hubapi.com/contacts/v1/lists/all/contacts/all?hapikey=" + hapikey + "&count=" + count + "&property=vid&property=email&property=firstname&property=lastname&property=hub_a_marketing_consent&property=lastmodifielddate";

                        resLink = "https://api.hubapi.com/contacts/v1/lists/recently_updated/contacts/recent?hapikey=" + hapikey + "&count=" + count + "&property=vid&property=email&property=firstname&property=lastname&property=hub_a_marketing_consent&property=lastmodifielddate";
                        //response = await client.GetAsync(resLink);
                        response = client.GetAsync(resLink).Result;
                        Console.WriteLine("res {0}", response);
                    }
                    else
                    {
                        BatchCountNumber++;
                        Console.WriteLine("BatchCountNumber {0}", BatchCountNumber);


                        //resLink = "https://api.hubapi.com/contacts/v1/lists/all/contacts/all?hapikey=" + hapikey + "&count=" + count + "&vidOffset=" + vidOffset;
                        //resLink = "https://api.hubapi.com/contacts/v1/lists/recently_updated/contacts/recent?hapikey=" + hapikey + "&count=" + count + "&property=hub_a_addfromdate&property=hub_a_dependantno&property=hub_a_dob&property=email&property=hub_a_employmentstatus&property=hub_a_employmenttown&property=hub_a_isloggedin&property=form_section_4&property=form_section_5&property=firstname&property=lastname&property=hub_a_lengthofloan&property=hub_a_lastchanged&property=hub_a_mobile&property=mobilephone&property=loan_id&property=loannumber&property=loan_stage&property=hub_a_maritalstatus&property=hub_a_marketing_consent&property=hub_a_monthlytakehome&property=hub_a_postcode&property=hub_a_title&property=hub_a_whenareyoupaid" + "&vidOffset=" + vidOffset + "&timeOffset=" + timeOffset;
                        //resLink = "https://api.hubapi.com/contacts/v1/lists/all/contacts/all?hapikey=" + hapikey + "&count=" + count + "&property=vid&property=email&property=firstname&property=lastname&property=hub_a_marketing_consent&property=lastmodifielddate" + "&vidOffset=" + vidOffset;

                        resLink = "https://api.hubapi.com/contacts/v1/lists/recently_updated/contacts/recent?hapikey=" + hapikey + "&count=" + count + "&property=vid&property=email&property=firstname&property=lastname&property=hub_a_marketing_consent&property=lastmodifielddate" + "&vidOffset=" + vidOffset + "&timeOffset=" + timeOffset;
                        //response = await client.GetAsync(resLink);
                        response = client.GetAsync(resLink).Result;
                    }

                    response.EnsureSuccessStatusCode();
                    //String responseBody = await response.Content.ReadAsStringAsync();
                    String responseBody = response.Content.ReadAsStringAsync().Result;
                    // Above three lines can be replaced with new helper method below
                    // string responseBody = await client.GetStringAsync(uri);

                    JObject contacts = JObject.Parse(responseBody);
                    String vidoffset = @"vid-offset";
                    String has_more = @"has-more";
                    String timeoffset = @"time-offset";

                    offset = (int)(contacts.GetValue(vidoffset));
                    has_more = (string)contacts.GetValue(has_more);
                    time_offset = (double)(contacts.GetValue(timeoffset));

                    vidOffset = offset;

                    //******************************************************************
                    //  Batch Conversion
                    //
                    //******************************************************************

                    Console.WriteLine("building table");
                    // Fill Datatable with each received batch
                    for (c = 0; c < count; c++)
                    {
                        var conn = contacts.SelectToken("contacts[" + c + "].properties.lastmodifieddate.value");
                        var conn1 = contacts.SelectToken("contacts[" + c + "]");

                        if (conn != null)
                        {
                            DateTime date1 = UnixTimestampToDateTime((double)conn);    // And lastupdated in HubSpot
                                                                                       //int result = DateTime.Compare(date1, Checkdate);           // var Checkdate comes from HubSpotLastStart(sqlcon);
                                                                                       // int result = 1;

                            if (runs % 2000 == 0)
                            {
                                //Console.WriteLine("12601 found");
                                Console.Write(contacts.SelectToken("contacts[" + c + "].vid") + " -> ");
                                Console.Write(contacts.SelectToken("contacts[" + c + "].properties.email.value") + " -> ");
                                Console.Write(contacts.SelectToken("contacts[" + c + "].properties.firstname.value") + " -> ");
                                Console.Write(contacts.SelectToken("contacts[" + c + "].properties.lastname.value") + " -> ");
                                Console.Write(contacts.SelectToken("contacts[" + c + "].properties.hub_a_marketing_consent.value") + " -> ");
                                Console.Write(contacts.SelectToken("contacts[" + c + "].identity-profiles[0].saved-at-timestamp") + " -> ");
                                Console.WriteLine(contacts.SelectToken("contacts[" + c + "].properties.lastmodifieddate.value"));
                            }


                            num++;
                            var conn_vid = contacts.SelectToken("contacts[" + c + "].vid");
                            var conn_email = contacts.SelectToken("contacts[" + c + "].properties.email.value");
                            var conn_first = contacts.SelectToken("contacts[" + c + "].properties.firstname.value");
                            var conn_last = contacts.SelectToken("contacts[" + c + "].properties.lastname.value");
                            var conn_Market_Consent = contacts.SelectToken("contacts[" + c + "].properties.hub_a_marketing_consent.value");
                            //if (conn_Market_Consent == null)
                            //    conn_Market_Consent = "false";
                            conn_createdate = (double)contacts.SelectToken("contacts[" + c + "].identity-profiles[0].saved-at-timestamp");
                            conn_lastmodifieddate = (double)contacts.SelectToken("contacts[" + c + "].properties.lastmodifieddate.value");

                            // 6.3  Add to Datatable
                            GetHubAppDataTableAll((int)conn_vid, (string)conn_email, (string)conn_first, (string)conn_last, (string)conn_Market_Consent, UnixTimestampToDateTime(conn_createdate), UnixTimestampToDateTime(conn_lastmodifieddate));
                        }
                    }
                    Console.WriteLine();


                    HubAppBulkInsertionSection(sqlcon, dtblup);
                    dtblup.Rows.Clear();
                    c = 0;


                    //*******************************************************************
                    //  Upload to Analytics table
                    //
                    //*******************************************************************

                    Console.WriteLine("lc : {0}",lc);
                    if (string.Compare(has_more, "True") == 0)
                    {
                        //Console.WriteLine("{0} Found, On to the next.", num);
                        Console.WriteLine("more to add to table");
                        lc = 0;
                        //HubBatch((int)offset, (double)time_offset);
                    }
                    else
                    {
                        lc = 1;
                        Console.WriteLine("***** Pre Insert Datatable *****");
                        Console.WriteLine("Retrieved All Recently Updated ");
                        Console.WriteLine("Found {0} entries ", num);

                        // Read Datatable dtblup
                        //********************************************************
                        Console.WriteLine();

                        Console.WriteLine("The last updated contacts have been retrieved");
                        Console.WriteLine("Datatable data table has been created from the recieved json");
                        //Console.WriteLine("DataTable From ReceivedJson In Database ***");
                        Console.WriteLine();
                        //********************************************************

                        // 6.4   Put DataTable (dtblup) From  ReceivedJson In Database
                        //********************************************************

                        Console.WriteLine("calling bulk Insert Checkloop:  {0}", checkloop);
                        HubAppBulkInsertionAll(sqlcon, dtblup);
                        Console.WriteLine("called bulk insert");

                        Console.WriteLine("Inserting Checkloop {0}", checkloop);
                        Console.WriteLine("Counting how many Checkstarts Occur:  {0}", checkstart);


                    }

                    /**********************************************************************/

                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
                    Console.ReadLine();
                }


            }

        }



        /*****************************************************************************************************************************************************************************************/
        // 4.0   Datatable -> Parse - > Format int Json "For Upload to Hubspot"
        //       DataTableToJSONWithJSONNet

        /*****************************************************************************************************************************************************************************************/

        public static void DataTableToJSONWithJSONNet(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(table);

            string jsonData3 = "{\"contacts\":" + JSONString + "}";         //  Create Json string
            JObject re = JObject.Parse(jsonData3);                          //  Turn Json string into object
            //Console.WriteLine("Returned datatable row Count : {0} : {1}", table.Rows.Count, re);

            //rowcount = table.Rows.Count;
            var rowcount = table.Rows.Count;
            var maxLoopCount = 0;
            var mod = 0;
            var start = 0;
            var stop = 0;
            var end = 1000;
            int i = 0;
     
            mod = rowcount % end;
            maxLoopCount = (rowcount - mod) / end;
            //Console.WriteLine("mod {0}, maxLoopCount {1}", mod, maxLoopCount);

            for (int x = 0; x < maxLoopCount; x++)
            {
                start = x * end;
                stop = start + end;
                for (i = start; i < stop; i++)
                {
                    //// 6.8
                    //if (contacts == null) { contacts = CreateContact(i, re); }
                    //else { contacts = contacts + CreateContact(i, re); }    //Get multiples of "end" eg 10's, 100's, 1000's  default 1000 
                    //                                                        //Console.WriteLine("i = " + i.ToString());

                    // 6.8
                    if (contacts == null) {
                        contacts = CreateContact(i, re);
                        vids = Getvid(i, re);
                    }
                    else { contacts = contacts + CreateContact(i, re);
                        vids = vids + Getvid(i, re);
                    }    
                }

                if (contacts != null)
                {
                    if (i == rowcount) { fin = true; }
                    UpdateHub(contacts, vids, fin);
                }
                contacts = null;
                vids = null;

                
                //Console.WriteLine(x * end);
                System.TimeSpan diffResult = startTime.Subtract(DateTime.Now);
               // Console.WriteLine("seconds run: " + diffResult.Seconds.ToString());
               // Console.WriteLine("minutes run: " + diffResult.Minutes.ToString());
            }
            //Console.WriteLine("big batches done");
            

            for (var j = i ; j < rowcount; j++)
            {
                // 6.8
                if (contacts == null) {
                    contacts = CreateContact(j, re);
                    vids = Getvid(j, re);
                }
                else { contacts = contacts + CreateContact(j, re); vids = vids + Getvid(j, re); }         //Get remainder eg 1 - 9 when end == 10, 1 - 999 when end = 1000. 
                //Console.WriteLine("j = " + j.ToString());
            }

            if (contacts != null)
            {
                fin = true;
                UpdateHub(contacts, vids, fin);
            }
            contacts = null;
            vids = null;

            if (rowcount == 0)
            {
                Console.WriteLine("ending process on database - Checkstop:  {0}", checkstop);
                HubSpotEnd(sqlcon, DateTime.Now, HubSpotStartId);
            }
        }

        /*****************************************************************************************************************************************************************************************/
        // 5.0 UpdateHubspot with Laps content
        //
        /*****************************************************************************************************************************************************************************************/

        //public static async void UpdateHub(string postBodyh, string CurrentVids, bool fin)
        public static void UpdateHub(string postBodyh, string CurrentVids, bool fin)
         {
            //string OutputField = string.Empty;

            if (postBodyh.Length > 0)
            {
                postBodyh = postBodyh.Remove(postBodyh.Length - 1);
                postBodyh = "[" + postBodyh + "]";
            }

            httpClientpost = new HttpClient();
            try
            {
                string resourceAddress = "https://api.hubapi.com/contacts/v1/contact/batch/?hapikey=" + hapikey;
                Console.WriteLine("about to call update api");
                httpClientpost.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //HttpResponseMessage wcfResponse = await httpClientpost.PostAsync(resourceAddress, new StringContent(postBodyh, Encoding.UTF8, "application/json"));
                HttpResponseMessage wcfResponse = httpClientpost.PostAsync(resourceAddress, new StringContent(postBodyh, Encoding.UTF8, "application/json")).Result;
                Console.WriteLine("update api called - about to display result");
                //await DisplayTextResult(wcfResponse, CurrentVids, postBodyh);
                DisplayTextResult(wcfResponse, CurrentVids, postBodyh);
                Console.WriteLine("HubSpot response: {0}", wcfResponse);

            }
            catch (HttpRequestException hre)
            {
                NotifyUser("Error:" + hre.Message);
            }
            catch (TaskCanceledException)
            {
                NotifyUser("Request canceled.");
            }
            catch (Exception ex)
            {
                NotifyUser(ex.Message);
            }

            if (fin)
            {
                Console.WriteLine("ending process on database - Checkstop:  {0}", checkstop);
                HubSpotEnd(sqlcon, DateTime.Now, HubSpotStartId);
            }
        }

        /******************************************************************************/
        // 5.1 DisplayTextResult from Api call
        //
        /******************************************************************************/

        static void HubUpdateFail(SqlConnection sqlcon, string currentVids, string sentJson, string responseBack)
        {
            //string responsJsonText = await Message.Content.ReadAsStringAsync();
            //string responsJsonText = Message.Content.ReadAsStringAsync().Result;
            Console.WriteLine("SentJson = {0}", sentJson);
            Console.WriteLine();
            Console.WriteLine("Vids = {0}", currentVids);
            Console.WriteLine();
            Console.WriteLine("responseBack = {0}", responseBack);
            Console.WriteLine();
            Console.WriteLine();

            //string text = "Sent Json \n " + sentJson + "\nVids \n" + currentVids + "\nresponseBack \n" + responseBack;
            //System.IO.File.WriteAllText(@"Desktop\ErrorResponseText.txt", text);

            // sp_HubErrorLog (vids, request, response)
            if (sqlcon.State == ConnectionState.Closed)
                sqlcon.Open();
            SqlCommand sqlcmd = new SqlCommand("sp_HubErrorLog", sqlcon);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.Parameters.AddWithValue("@vids", currentVids);
            sqlcmd.Parameters.AddWithValue("@jsRq", sentJson);
            sqlcmd.Parameters.AddWithValue("@jsRsp", responseBack);
            //sqlcmd.CommandTimeout = 0;
            sqlcmd.ExecuteNonQuery();
            sqlcon.Close();
      
            Console.WriteLine("Hubspot sp_HubErrorLog has been called");
        }


        /******************************************************************************/
        // 6.0 Utilities 
        // 6.1 UnixTimestampToDateTime
        // 6.2 DisplayTextResult from Api call
        // 6.3 Put Received Json into Datatable
        // 6.4 Upload Datatable to Uncle Database, Return Data to Modify Hubspot
        // 6.5 HubSpot Start
        // 6.6 HubSpot End
        // 6.7 HubSpot LastStart
        // 6.8 HubSpot CreateContact
        // 6.8.1 HubSpot CreateContact vid list
        // 6.9 Upload Datatable to Uncle Database, Return Data to Modify Hubspot
        /******************************************************************************/


        /******************************************************************************/
        // 6.1  UnixTimestampToDateTime
        //
        /******************************************************************************/

        public static DateTime UnixTimestampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }


        public static long ConvertToUnixTime(DateTime datetime)
        {
            DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            return (long)(datetime - sTime).TotalMilliseconds;
        }

        /******************************************************************************/
        // 6.2 DisplayTextResult from Api call
        //
        /******************************************************************************/

        //static async Task DisplayTextResult(HttpResponseMessage Message, string currentvids, string currentJson)
        public static void DisplayTextResult(HttpResponseMessage Message, string currentvids, string currentJson)
        {
            //string responsJsonText = await Message.Content.ReadAsStringAsync();
            string responsJsonText = Message.Content.ReadAsStringAsync().Result;
            //string responsJsonText = Message.Content.ReadAsStringAsync().Result;
            //Console.WriteLine("responsJsonText = {0}", responsJsonText);

            //new stuff

            if (responsJsonText.Length > 0)
            {
                HubUpdateFail(sqlcon, currentvids, currentJson, responsJsonText);
            }
        }

        /******************************************************************************/
        // 6.3 Put Received Json into Datatable
        //
        /******************************************************************************/

        static DataTable GetHubAppDataTableAll(int vid, string email, string firstname, string lastname, string marketing_consent, DateTime createdate, DateTime lastmodifieddate)
        {
            dtblup.Rows.Add(vid, email, firstname, lastname, marketing_consent, createdate, lastmodifieddate);
            string upString = JsonConvert.SerializeObject(dtblup);
            string jsonData = "{\"Upload\":" + upString + "}";                        //  Create Json string
            JObject re = JObject.Parse((string)jsonData);                             //  Turn Json string into object
                                                                                      //  Console.WriteLine("Row Count : {0} ", dtblup.Rows.Count);
            return dtblup;
        }

        /******************************************************************************/
        // 6.4 Upload Datatable to Uncle Database, Return Data to Modify Hubspot
        //
        /******************************************************************************/

        static void HubAppBulkInsertionAll(SqlConnection sqlcon, DataTable HubspotApplications)
        {
            Console.WriteLine("Receiving  Information");
            DataTable table = new DataTable();
            if (sqlcon.State == ConnectionState.Closed)
                sqlcon.Open();
            SqlCommand sqlcmd = new SqlCommand("HubAppBulkInsertion", sqlcon);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            //sqlcmd.Parameters.AddWithValue("@app", HubspotApplications);
            sqlcmd.CommandTimeout = 0;


            //Console.WriteLine("Receiving information");
            using (var da = new SqlDataAdapter(sqlcmd))
            {
                //Console.WriteLine("Filling information");
                da.Fill(table);
            }

            Console.WriteLine(table);
            // 4.0
            DataTableToJSONWithJSONNet(table);

            //sqlcmd.ExecuteNonQuery();

            sqlcon.Close();
            checkloop++;
        }

        /******************************************************************************/
        // 6.5 HubSpot Start
        //
        /******************************************************************************/

        static int HubSpotStart(SqlConnection sqlcon, DateTime start)
        {
            if (sqlcon.State == ConnectionState.Closed)
                sqlcon.Open();
            SqlCommand sqlcmd = new SqlCommand("sp_HubJobSchedulerStart", sqlcon);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            //sqlcmd.CommandTimeout = 0;
            sqlcmd.Parameters.AddWithValue("@Dt", start);
            
            //Create return parameter
            SqlParameter retval = sqlcmd.Parameters.Add("@startID", SqlDbType.Int);
            retval.Direction = ParameterDirection.ReturnValue;
            
            //Execute Query
            sqlcmd.ExecuteNonQuery();

            //Select return parameter
            int lastInsertedId = (int)sqlcmd.Parameters["@startID"].Value;
            sqlcon.Close();

            checkstart++;

            return lastInsertedId;
        }

        /******************************************************************************/
        // 6.6 HubSpot End
        //
        /******************************************************************************/

        static void HubSpotEnd(SqlConnection sqlcon, DateTime end, int id)
        {
            Console.WriteLine("About to end process");
            if (sqlcon.State == ConnectionState.Closed)
                sqlcon.Open();
            SqlCommand sqlcmd = new SqlCommand("sp_HubJobSchedulerEnd", sqlcon);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.Parameters.AddWithValue("@Dt", end);
            sqlcmd.Parameters.AddWithValue("@startID", id);
            //sqlcmd.CommandTimeout = 0;
            Console.WriteLine("About to call final SP");
            sqlcmd.ExecuteNonQuery();
            Console.WriteLine("final SP called");
            sqlcon.Close();
            checkstop++;
            Console.WriteLine("Hubspot has ended");
            Console.WriteLine("process completed - Checkstop:  {0}", checkstop);
            //Console.ReadLine();
        }

        /******************************************************************************/
        // 6.7 HubSpot LastStart
        //
        /******************************************************************************/

        static DateTime HubSpotLastStart(SqlConnection sqlcon)
        {
            //Console.WriteLine("going into connection");
            using (SqlCommand cmd = new SqlCommand("sp_HubJobSchedulerLastStart", sqlcon))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.CommandTimeout = 0;
                if (sqlcon.State == ConnectionState.Closed)
                    sqlcon.Open();
                //Console.WriteLine("connected");
                //cmd.ExecuteNonQuery();
                //Console.WriteLine("called db");
                //Console.WriteLine("closing connection");

                using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.Default))
                {
                    //Console.WriteLine("reading rdr");
                    if (rdr.HasRows)
                    {
                        //Console.WriteLine("has rows");
                        while (rdr.Read())
                        {
                            last = (DateTime)rdr["LastStartedDate"];
                            //Console.WriteLine(last.ToString());
                            //Console.WriteLine("Last Start While loop");
                            //Console.WriteLine("rdr : {0}", rdr.ToString());

                        }
                    }
                }
                sqlcon.Close();
                //Console.WriteLine("closing connection");
            }
            return last;
        }

        /******************************************************************************/
        // 6.8 HubSpot CreateContact
        //
        /******************************************************************************/
        static string CreateContact(int i, JObject re)
        {
            H_Vid = (string)re.SelectToken("contacts[" + i + "].H_Vid");                                  // Vid
            P_mktOptIn = (string)re.SelectToken("contacts[" + i + "].P_mktOptIn");                        // marketing opt-in
            P_sumFundedLoan = (string)re.SelectToken("contacts[" + i + "].P_sumFundedLoan");              // Life time value
            P_nbFundedLoan = (string)re.SelectToken("contacts[" + i + "].P_nbFundedLoan");                // Number of loans
            LF_LoanNo = (string)re.SelectToken("contacts[" + i + "].LF_LoanNo");                          // Loan number
            LF_Instalments = (string)re.SelectToken("contacts[" + i + "].LF_Instalments");                // Loan length
            LF_Balance = (string)re.SelectToken("contacts[" + i + "].LF_Balance");                        // outstanding balance 
            LF_FundedDate = (string)re.SelectToken("contacts[" + i + "].LF_FundedDate");                  // funded date
            LF_LastInstDate = (string)re.SelectToken("contacts[" + i + "].LF_LastInstDate");              // due date
            LF_RepaidDate = (string)re.SelectToken("contacts[" + i + "].LF_RepaidDate");                  // repaid date
            LF_High_ArrearsLevel = (string)re.SelectToken("contacts[" + i + "].LF_High_ArrearsLevel");    // ArrearsLevel
            LA_StageStatus = (string)re.SelectToken("contacts[" + i + "].LA_StageStatus");                // StageStatus
            LA_ExcludeFromLending = (string)re.SelectToken("contacts[" + i + "].LA_ExcludeFromLending");  // excluded from lending 

            if (LF_FundedDate != null)
            {
                try
                {
                    //DateTime FundedDate = new DateTime(Int32.Parse(LF_FundedDate.Substring(0, 4)), Int32.Parse(LF_FundedDate.Substring(5, 2)), Int32.Parse(LF_FundedDate.Substring(8, 2)), Int32.Parse(LF_FundedDate.Substring(11, 2)), Int32.Parse(LF_FundedDate.Substring(14, 2)), Int32.Parse(LF_FundedDate.Substring(17, 2)), Int32.Parse(LF_FundedDate.Substring(20, 3)));
                    DateTime FundedDate = DateTime.ParseExact(LF_FundedDate, "MM/dd/yyyy HH:mm:ss", null);
                    LF_FundedDate = ConvertToUnixTime(FundedDate).ToString();
                }
                catch { LF_FundedDate = null; }
            }

            if (LF_RepaidDate != null)
            {
                try
                {
                    //DateTime RepaidDate = new DateTime(Int32.Parse(LF_RepaidDate.Substring(0, 4)), Int32.Parse(LF_RepaidDate.Substring(5, 2)), Int32.Parse(LF_RepaidDate.Substring(8, 2)), Int32.Parse(LF_RepaidDate.Substring(11, 2)), Int32.Parse(LF_RepaidDate.Substring(14, 2)), Int32.Parse(LF_RepaidDate.Substring(17, 2)), Int32.Parse(LF_RepaidDate.Substring(20, 3)));
                    DateTime RepaidDate = DateTime.ParseExact(LF_RepaidDate, "MM/dd/yyyy HH:mm:ss", null);
                    LF_RepaidDate = ConvertToUnixTime(RepaidDate).ToString();
                }
                catch { LF_RepaidDate = null; }
            }

            if (LF_LastInstDate != null)
            {
                try
                {
                    //DateTime LastInstDate = new DateTime(Int32.Parse(LF_LastInstDate.Substring(0, 4)), Int32.Parse(LF_LastInstDate.Substring(5, 2)), Int32.Parse(LF_LastInstDate.Substring(8, 2)), Int32.Parse(LF_LastInstDate.Substring(11, 2)), Int32.Parse(LF_LastInstDate.Substring(14, 2)), Int32.Parse(LF_LastInstDate.Substring(17, 2)), Int32.Parse(LF_LastInstDate.Substring(20, 3)));
                    DateTime LastInstDate = DateTime.ParseExact(LF_LastInstDate, "MM/dd/yyyy HH:mm:ss", null);
                    LF_LastInstDate = ConvertToUnixTime(LastInstDate).ToString();
                }
                catch { LF_LastInstDate = null; }
            }



            contact = "{" +
                            "\"vid\":\"" + H_Vid + "\"," +
                            "\"properties\": [" +
                            "{\"property\":\"hub_a_marketing_consent\",\"value\": \"" + P_mktOptIn + "\"}," +
                            "{\"property\":\"p_sumfundedloan\",\"value\": \"" + P_sumFundedLoan + "\"}," +
                            "{\"property\":\"p_nbfundedloan\",\"value\": \"" + P_nbFundedLoan + "\"}," +
                            "{\"property\":\"lf_loanno\",\"value\": \"" + LF_LoanNo + "\"}," +
                            "{\"property\":\"lf_instalments\",\"value\": \"" + LF_Instalments + "\"}," +
                            "{\"property\":\"lf_balance\",\"value\": \"" + LF_Balance + "\"}," +
                            "{\"property\":\"lf_fundeddate\",\"value\": \"" + LF_FundedDate + "\"}," +
                            "{\"property\":\"lf_lastinstdate\",\"value\": \"" + LF_LastInstDate + "\"}," +
                            "{\"property\":\"lf_repaiddate\",\"value\": \"" + LF_RepaidDate + "\"}," +
                            "{\"property\":\"lf_high_arrearslevel\",\"value\": \"" + LF_High_ArrearsLevel + "\"}," +
                            "{\"property\":\"la_stagestatus\",\"value\": \"" + LA_StageStatus + "\"}," +
                            "{\"property\":\"la_excludefromlending\",\"value\": \"" + LA_ExcludeFromLending + "\"}" +
                            "]" +
                            "},";

            if (i % 1 == 0) { Console.WriteLine("contact i(" + i.ToString() + "): " + contact.ToString()); }
            return contact;
        }

        /******************************************************************************/
        // 6.8.1 HubSpot CreateContact vid list
        //
        /******************************************************************************/
        static string Getvid(int i, JObject re)
        {
            H_Vid = (string)re.SelectToken("contacts[" + i + "].H_Vid");                                  // Vid
            
            vid = H_Vid + ",";
         
            return vid;
        }

        /******************************************************************************/
        // 6.9 Upload Datatable to Uncle Database, Return Data to Modify Hubspot
        //
        /******************************************************************************/

        static void HubAppBulkInsertionSection(SqlConnection sqlcon, DataTable HubspotApplications)
        {
            //Console.WriteLine("Sending information");
            DataTable table = new DataTable();
            if (sqlcon.State == ConnectionState.Closed)
                sqlcon.Open();
            SqlCommand sqlcmd = new SqlCommand("sp_HubApplications", sqlcon);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.Parameters.AddWithValue("@app", HubspotApplications);
            //sqlcmd.CommandTimeout = 0;


            ////Console.WriteLine("Receiving information");
            //using (var da = new SqlDataAdapter(sqlcmd))
            //{
            //    //Console.WriteLine("Filling information");
            //    da.Fill(table);
            //}

            //Console.WriteLine(table);
            //// 4.0
            //DataTableToJSONWithJSONNet(table);

            //sqlcmd.ExecuteNonQuery();
            try
            {
                sqlcmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(" sp_HubApplications Exception :" + ex);
                sqlcmd.ExecuteNonQuery();
            }

            sqlcon.Close();
            checkloop++;
        }


        /*******************************************************************************************************************/
        // Notify User

        /*******************************************************************************************************************/

        private static void NotifyUser(string Note)
        {
            Console.WriteLine(Note);
        }
  
    }
}


